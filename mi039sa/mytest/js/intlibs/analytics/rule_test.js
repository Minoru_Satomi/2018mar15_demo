window.rtc = window.rtc || {};
window.rtc.utils = window.rtc.utils || {};
RTCConfig.analyticsTrackEvents = true;

$.extend( true, rtc.utils, {
	analytics: {
		rules: [
				//{
				//	action: 'video-started'
				//},
				//{
				//	action: 'video35',
				//	fnc: function(value) { return JSON.parse(value.data);}
				//},
				//{
				//	action: 'card.button.click',
				//	fnc: function(value) { return JSON.parse(value.data);}
				//}
                ],
		trackEvent: function(category, action, name, value) {
			if( 	action == 'video-started' || action =='card.button.click' || 
				action == 'video35' || (action == 'card.show' && value.card == 'End') ) {
				var array = [action, name, RTCVisit.uid, value.data];
				window.parent.postMessage(  array,'*');
				//window.parent.postMessage(  array,'http://161.228.7.137');
				//window.parent.postMessage(  action + '>>' +  name + '>>' + RTCVisit.uid + '>>' + value.data,'http://161.228.7.137');
			}
		}
        }
})
